// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "PaintBallGameMode.h"
#include "PaintBallHUD.h"
#include "PaintBallCharacter.h"
#include "UObject/ConstructorHelpers.h"

APaintBallGameMode::APaintBallGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = APaintBallHUD::StaticClass();
}
