// Fill out your copyright notice in the Description page of Project Settings.


#include "FOW.h"

// Sets default values
AFOW::AFOW()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFOW::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFOW::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

